let expect = require('chai').expect;
let GameTicTacToe_Base = require('../src/Games/GameTicTacToe_base');

describe('GameTicTacToe_base', () => {
  it('Initial Field', () => {
    let game = new GameTicTacToe_Base();
    expect(game.GetField()).to.eql([
      '.','.','.',
      '.','.','.',
      '.','.','.'
      ])
  })

  it('Inactive Player is forbidden to pick', () => {
    let game = new GameTicTacToe_Base();
    game.activePlayer = 0;

    expect(game.PickField(1, 1, 1)).to.be.false;
    expect(game.GetField()).to.eql([
      '.','.','.',
      '.','.','.',
      '.','.','.'
      ])
  })

  it('Active Player is allowed to pick', () => {
    let game = new GameTicTacToe_Base();
    game.activePlayer = 0;

    expect(game.PickField(1, 1, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      '.','.','.',
      '.','X','.',
      '.','.','.'
      ])
  })

  it('Picked field cant be repicked', () => {
    let game = new GameTicTacToe_Base();
    game.activePlayer = 0;

    expect(game.PickField(1, 1, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      '.','.','.',
      '.','X','.',
      '.','.','.'
      ])

    expect(game.PickField(1, 1, game.GetPlayerActiveIdx())).to.be.false;
    expect(game.GetField()).to.eql([
      '.','.','.',
      '.','X','.',
      '.','.','.'
      ])

    expect(game.PickField(1, 0, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      '.','O','.',
      '.','X','.',
      '.','.','.'
      ])
  })

  it('Play a short round', () => {
    let game = new GameTicTacToe_Base();
    game.activePlayer = 0;

    expect(game.PickField(1, 1, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      '.','.','.',
      '.','X','.',
      '.','.','.'
      ])

    expect(game.PickField(1, 0, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      '.','O','.',
      '.','X','.',
      '.','.','.'
      ])

    expect(game.PickField(0, 0, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      'X','O','.',
      '.','X','.',
      '.','.','.'
      ])
  })
})