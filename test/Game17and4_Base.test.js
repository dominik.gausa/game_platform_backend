let expect = require('chai').expect;
let Game17and4_Base = require('../src/Games/Game17and4_Base');


describe('Game17and4_Base', () => {
  it('Basic Game', () => {
    let game = new Game17and4_Base();
    expect(game.cards.length).to.eq(52);
    game.cards = [
      {
        color: 'Diamonds',
        name: '10',
        value: 10
      },
      {
        color: 'Diamonds',
        name: 'Queen',
        value: 10
      },
      {
        color: 'Spades',
        name: 'King',
        value: 10
      },
      {
        color: 'Hearts',
        name: 'Ass',
        value: 11
      },
      {
        color: 'Spades',
        name: '2',
        value: 2
      },
      {
        color: 'Hearts',
        name: '10',
        value: 10
      }
    ];

    expect(game.GetData()).to.eql({
      players: []
    });

    expect(game.PlayerAction(game.GetPlayerActiveIdx())).to.be.true;
    expect(game.PlayerAction(game.GetPlayerActiveIdx())).to.be.true;

    game.NextRound();
    expect(game.PlayerAction(game.GetPlayerActiveIdx())).to.be.true;
    expect(game.PlayerAction(game.GetPlayerActiveIdx())).to.be.true;

    expect(game.GetData()).to.eql({
        players: [
            {
              cards: [
                {
                  color: 'Hearts',
                  name: '10',
                  value: 10
                },
                {
                  color: 'Hearts',
                  name: 'Ass',
                  value: 11
                }
              ],
              cash: 100,
              value: 21,
              wage: 10
            },
            {
              cards: [
                {
                  color: 'Spades',
                  name: '2',
                  value: 2
                },
                {
                  color: 'Spades',
                  name: 'King',
                  value: 10
                }
              ],
              cash: 100,
              value: 12,
              wage: 10
            }
          ]
    });

    game.NextRound();
    game.NextRound();
  })
})