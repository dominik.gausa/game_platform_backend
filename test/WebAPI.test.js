var expect = require('chai').expect
  , io = require('socket.io-client')
  , ioOptions = { 
      transports: ['websocket']
    , forceNew: true
    , reconnection: false
  }
  , testMsg = 'HelloWorld'



let server = require('../src/index')
const serverUrl = 'http://localhost:' + server.app.get('port') + '/';

class TestClients {
  constructor(number) {
    this.users = []
    while(number-- > 0)
      this.users.push(io(serverUrl, ioOptions))

    console.log(`Created ${this.users.length} TestUsers`)
  }

  AuthAll(done) {
    return Promise.all(this.users.map((user, idx) => {
      let credentials = {user: 'User'+idx, pass: 'pass' + idx}
      console.log(`Authenticate ${JSON.stringify(credentials)}`)
      return this.Auth(user, credentials)
    }))
  }

  Auth(user, credentials) {
    return new Promise((res, rej) => {
      user.on('auth', msg => {
        expect(msg).to.equal('OK')
        user.removeAllListeners('auth')
        res()
      })
      user.emit('auth',credentials)
    })
  }

  CreateRoom(user, room, type) {
    return new Promise((resolve, reject) => {
      user.on('room', result => {
        user.removeAllListeners('room')
        if(result == 'OK') resolve()
        else reject('Room join not OK')
      })
      user.emit('room', {request: 'create', room, type})
    })
  }

  JoinRoom(user, room, type) {
    return new Promise((resolve, reject) => {
      user.on('room', result => {
        user.removeAllListeners('room')
        if(result == 'OK') resolve()
        else reject('Room join not OK')
      })
      user.emit('room', {request: 'join', room, type})
    })
  }

  Disconnect() {
    console.log(`Disconnect ${this.users.length} TestUsers`)
    this.users.forEach(user => {
      user.disconnect()
    })
  }
}


describe('Chat Events', () => {
  let clients
  before(() => {
    server.start()
    clients = new TestClients(3);
  })

  after(() => {
    clients.Disconnect()
    server.server.close()
  })

  describe('Global Message Events', () => {
    it('Server shall echo data on Ping', done => {
      clients.users[0].on('ping', data => {
        expect(data).to.equal('pong')
        done()
      });
      clients.users[0].emit('ping', 'pong')
    })

    it('Clients should receive a message when the `message` event is emited.', done => {
      clients.users[1].on('message', msg => {
        expect(msg).to.equal(testMsg)
        done()
      })
      clients.users[0].emit('message', {room: 'global', msg: testMsg})
    })
  })
})

describe('Room Basics', () => {
  let clients
  beforeEach(done => {
    server.start()
    clients = new TestClients(5);
    clients.AuthAll()
      .then(() => clients.CreateRoom(clients.users[0], '1234', 'base'))
      .then(() => clients.JoinRoom(clients.users[1], '1234'))
      .then(() => done())
  })

  afterEach(() => {
    clients.Disconnect()
    server.server.close()
  })

  it('Send to Room', done => {
    clients.users[2].on('message', msg => {
      done('user3 shouldnt get any message')
    })

    clients.users[1].on('message', msg => {
      expect(msg).to.equal(testMsg)
      done()
    })
    
    clients.users[0].emit('message', {room: '1234', msg: testMsg})
  })

  it('Send to Room 2', done => {
    clients.users[2].on('message', msg => {
      done('user3 shouldnt get any message')
    })

    clients.users[0].on('message', msg => {
      expect(msg).to.equal(testMsg)
      done()
    })
    
    clients.users[1].emit('message', {room: '1234', msg: testMsg})
  })

  it('Wait for room tick', done => {
    clients.users[0].on('game', msg => {
      console.log('User0', msg)
      if(msg.type == 'tick') done()
    })
  })
})

describe('Play Catch', () => {
  let clients
  beforeEach(done => {
    server.start()
    clients = new TestClients(5);
    clients.AuthAll()
      .then(() => clients.CreateRoom(clients.users[0], '1235', 'catch'))
      .then(() => clients.JoinRoom(clients.users[1], '1235'))
      .then(() => done())
  })

  afterEach(() => {
    clients.Disconnect()
    server.server.close()
  })

  it('Play Game', function (done) {
    clients.users[0].on('game', msg => {
      console.log('User0', msg)
      if(msg.type == 'info'){
        clients.users[0].emit('room', {room: '1235', request: 'action', data: 'Catched ya'})
      }
    })

    clients.users[1].on('game', msg => {
      console.log('User1', msg)
      if(msg.type == 'info'){
        done()
      }
    })

    clients.users[2].on('game', msg => {
      console.log('User2', msg)
      done('Error: User2 is not in room')
    })

    clients.users[2].on('message', msg => {
      console.log('User2', msg)
      done('Error: User2 is not in room')
    })
  })
})
