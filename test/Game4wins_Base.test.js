let expect = require('chai').expect;
let Game4wins_base = require('../src/Games/Game4wins_base');


describe('Game4wins_base', () => {
  it('Initial Field', () => {
    let game = new Game4wins_base();
    expect(game.GetField()).to.eql([
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.']
    ])
  })

  it('Inactive Player is forbidden to pick', () => {
    let game = new Game4wins_base();
    let playerPassive = [1, 0][game.GetPlayerActiveIdx()];
    expect(game.PickField(1, playerPassive)).to.be.false;
    expect(game.GetField()).to.eql([
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.']
    ])
  })

  it('Active Player is allowed to pick', () => {
    let game = new Game4wins_base();
    game.activePlayer = 0;

    expect(game.PickField(1, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', 'X', '.', '.', '.', '.', '.']
    ])
  })

  it('Play a short round', () => {
    let game = new Game4wins_base();
    game.activePlayer = 0;

    expect(game.PickField(1, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', 'X', '.', '.', '.', '.', '.']
    ]);

    expect(game.PickField(2, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', 'X', 'O', '.', '.', '.', '.']
    ]);

    expect(game.PickField(1, game.GetPlayerActiveIdx())).to.be.true;
    expect(game.GetField()).to.eql([
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', 'X', '.', '.', '.', '.', '.'],
      ['.', 'X', 'O', '.', '.', '.', '.']
    ]);
  })
})