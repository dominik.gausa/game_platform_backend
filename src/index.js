const app = require('express')();
const server = require('http').createServer(app)
const sockets = require('./sockets')
const debug = require('debug')
const log = debug('App')

// Register Gametypes
const GameBase = require('./Games/GameBase')
const GameCatch = require('./Games/GameCatch')
const GameTicTacToe = require('./Games/GameTicTacToe')
const Game4wins = require('./Games/Game4wins')
const Game = require('./Game')

Game.RegisterGameType('base', (name) => new GameBase(name))
Game.RegisterGameType('catch', (name) => new GameCatch(name))
Game.RegisterGameType('TicTacToe', (name) => new GameTicTacToe(name))
Game.RegisterGameType('4wins', (name) => new Game4wins(name))


// Set Debuglevels
debug.enable('App')
debug.enable('*')


// Startup server and components

app.set('port', process.env.PORT || 3001)
sockets.SetupSockets(server, 3000); //app.get('port'))


function start() {
  server.listen(app.get('port'), () => {
    log('Server started on port ' + app.get('port'));
    server.once('close', () => Game.CloseAllGames())
  });
}
// export the server so it can be easily called for testing
module.exports = {server, app, start}
