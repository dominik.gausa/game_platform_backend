const socketio = require('socket.io');
const log = require('debug')('WebSocket')
const logConnection = require('debug')('WebSocket:Connection')
const logPing = require('debug')('WebSocket:Ping')
const logAuth = require('debug')('WebSocket:Auth')
const logMessage = require('debug')('WebSocket:Message')
const logRoom = require('debug')('WebSocket:Room')
const logError = require('debug')('WebSocket:Error')
const Game = require('./Game')

/**
 * API
 * auth
 *  - Authentication
 * ping
 *  - echo data back for debugging
 * room
 *   {room, request [, type]}
 *   - room: room name
 *   - request:
 *     join: Joins or creates a game, may set type
 *     action: forwards "data" to current game
 * message
 *   - Chatmessage within room
 * */

 function MiddlewareRoom(socket) {
  return data => {
    logRoom('%o', {socket: socket.id, data})
    let game
    switch(data.request) {
      case 'list':
        socket.emit('room', {rooms: Game.ListGames()})
        break;
      case 'create':
        game = Game.GetGame(data.room)
        if(!game) {
          logRoom(`Starting Room ${data.room}`)
          game = Game.CreateGame(data.room, data.type)
        }else{
          logError(`Game already running in ${data.room}`)
          socket.emit('room', {error: 'Game already running'})
          break;
        }
        // Falling through
      case 'join':
        game = game || Game.GetGame(data.room)
        if(game) {
          game.SetRoom(io.in(data.room))
          game.Join(socket.data, socket)
          socket.games.push(game)
          socket.join(data.room);
          socket.emit('room', 'OK')
        }else{
          socket.emit('room', {error: 'Game doesnt exist'})
        }
        break;
      case 'action':
        game = Game.GetGame(data.room)
        if(game) {
          game.ReceiveFromPlayer(socket.data, data.data)
        }else{
          logError(`No Game in Room ${data.room}`)
          socket.emit('room', {error: 'No game in this room'})
        }
        break;
      default:
        logError('unknown Room request')
        socket.emit('room', {error: 'unknown Room request'})
    }
  }
}

function MiddlewareMessage(socket) {
  return msg => {
    logMessage('%o', {socket: socket.id, msg})
    if(socket.rooms.has(msg.room))
      io.to(msg.room).emit('message', msg.msg)
    else {
      logMessage('%o', {socket: socket.id, error: 'Trying to send to non joined room'})
      socket.emit('room', {error: 'Trying to send to non joined room'})
    }
  }
}


let io
function SetupSockets(server, port) {
  io = socketio(server, {
    cors: {
      origin: `http://localhost${port ? ':' + port : ''}`,
      methods: ["GET", "POST"]
    }
  })

  io.sockets.on('connection', socket => {
    logConnection(`newConnection %o`, socket.id);
    socket.join('global');
    socket.games = []
    
    socket.on("disconnecting", reason => {
      logConnection('disconnecting %o', {id: socket.id, rooms: socket.rooms});
      socket.games.forEach(game => game.Leave(socket.data.name, socket))
    })
    socket.on("disconnect", reason => {
      logConnection('disconnect %o', {id: socket.id});
    })

    socket.on('ping', data => {
      logPing('%o', {socket: socket.id, data})
      socket.emit('ping', data)
    })

    socket.on('auth', data => {
      logAuth('%o', {socket: socket.id, data})
      socket.data = {
        name: data.user,
        uuid: data.uuid
      }
      socket.emit('auth', 'OK')
      
      socket.on('room', MiddlewareRoom(socket))

      socket.on('message', MiddlewareMessage(socket))
    })

  })
}


module.exports = {
  SetupSockets
}