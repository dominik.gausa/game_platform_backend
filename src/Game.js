const log = require('debug')('Game')
const logError = require('debug')('Game:Error')


let gameList = {}
let GameTypes = {}

function RegisterGameType(type, creatorcb) {
  GameTypes[type] = creatorcb;
}

function GetGame(name) {
  return gameList[name];
}

function CreateGame(name, type) {
  if(gameList[name])
    log(`Recreating Game ${this.name}`)

  if(GameTypes[type]){
    gameList[name] = GameTypes[type](name)
  }else{
    logError(`Unknown gametype '${type}`)
  }
  return gameList[name];
}

function CloseAllGames() {
  let games = Object.values(gameList)
  games.forEach(g => g && g.Stop())
}

function Remove(name) {
  gameList[name] = undefined
}

function ListGames() {
  return Object.entries(gameList)
    .filter(e => e[1])
    .map(e => { return {room: e[0], type: e[1].GetType()}})
}

module.exports = {
  RegisterGameType,
  GetGame,
  CreateGame,
  CloseAllGames,
  Remove,
  ListGames
}
