const log = require('debug')('17and4')
const logError = require('debug')('17and4:Error')


class Game17and4_Base {
  constructor() {
    this.Reset();
  }

  Reset() {
    this.cards = [];
    [
      { name: 'Ass', value: 11 },
      { name: 2, value: 2 },
      { name: 3, value: 3 },
      { name: 4, value: 4 },
      { name: 5, value: 5 },
      { name: 6, value: 6 },
      { name: 7, value: 7 },
      { name: 8, value: 8 },
      { name: 9, value: 9 },
      { name: 10, value: 10 },
      { name: 'Jack', value: 10 },
      { name: 'Queen', value: 10 },
      { name: 'King', value: 10 },
    ].forEach(card => {
      this.cards.push({ ...card, color: 'Clubs' });
      this.cards.push({ ...card, color: 'Diamonds' });
      this.cards.push({ ...card, color: 'Hearts' });
      this.cards.push({ ...card, color: 'Spades' });
    });

    this.players = [];
    this.activePlayer = 0;
  }

  GetType() {
    return "17and4"
  }

  GetData() {
    return { players: this.players };
  }

  GetMaxPlayers() {
    return 2;
  }

  GetPlayerActiveIdx() {
    return this.activePlayer;
  }

  NextPlayer() {
    this.activePlayer++
    this.activePlayer %= this.GetMaxPlayers();
  }

  NextRound() {
    if(!this.actionHappened) { // Assuming all are happy
      this.CreateIfNotExist(-1);
      while(this.players[-1].value < 17) 
        this.PlayerDrawCard(-1);

      let bank = this.players[-1];
      let winners = [];
      if(this.players[-1].value > 21) {
        winners = this.players;
      }else if(this.players[-1].value == 21) {
      }else {
        this.players.forEach(p => {
          if(p.value == 21) {
            p.cash += p.wage * 1.5;
          }else if(p.value <= 21 
              || p.value >= 17) {
          }else {
            if(this.players[-1].value < p.value) {
              p.cash += p.wage * 1;
            }else if(this.players[-1].value > p.value) {

            }
          }
        });

        this.players.forEach(p => p.cards = [])
      }

      log(`Result: `, {bank, player: this.players})
    }
    this.activePlayer = 0;
    this.actionHappened = false;
  }

  CreateIfNotExist(playerIdx){
    if (!this.players[playerIdx])
      this.players[playerIdx] = { cards: [], value: 0, cash: 100, wage: 10 };
  }

  PlayerDrawCard(playerIdx) {
    this.players[playerIdx].cards.push(this.cards.pop());
    this.actionHappened = true;

    this.players[playerIdx].value = 0;
    this.players[playerIdx].cards.forEach(c => this.players[playerIdx].value += c.value);
}

  PlayerAction(playerIdx, action) {
    if (playerIdx == this.GetPlayerActiveIdx()) {
      this.CreateIfNotExist(playerIdx);

      if (this.players[playerIdx].value < 17
          || action == 'draw') {
        this.PlayerDrawCard(playerIdx);
      }

      this.NextPlayer();
    } else {
      return false;
    }

    return true;
  }
}

module.exports = Game17and4_Base
