const log = require('debug')('TicTacToe')
const logError = require('debug')('TicTacToe:Error')

class GameTicTacToe_Base {
  constructor() {
    this.Reset();
  }

  Reset() {
    this.field = [
      '.','.','.',
      '.','.','.',
      '.','.','.'
      ];
    this.activePlayer = Math.floor(Math.random() * Math.floor(this.GetMaxPlayers()));
  }

  GetType() {
    return "TicTacToe"
  }

  GetField() {
    return this.field;
  }

  GetMaxPlayers() {
    return 2;
  }

  GetPlayerActiveIdx() {
    return this.activePlayer;
  }

  NextPlayer() {
    this.activePlayer++
    this.activePlayer %= this.GetMaxPlayers();
  }

  PickField(x, y, playerIdx) {
    if(x < 0 && x >= 3) return false;
    if(y < 0 && y >= 3) return false;
    if(playerIdx != this.GetPlayerActiveIdx()) return false;
    if(this.field[y*3 + x] != '.') return false;

    this.field[y*3 + x] = ['X', 'O'][playerIdx];
    this.NextPlayer();
    return true;
  }
}

module.exports = GameTicTacToe_Base
