const log = require('debug')('Game')
const logError = require('debug')('Game:Error')
const Game = require('../Game')

/**
 * Game base
 * 
 * Derive your games from this and override Tick and ReceiveFromPlayer methods
 */
class GameBase {
  constructor(name) {
    this.name = name
    this.roomSocket = undefined
    this.players = []
    this.roomTickCount = 0

    this.handleInterval = setInterval(() => this.Tick(), 1000)
    log(`Starting ${this.GetType()} "${this.name}"`)
  }

  GetType() {
    return "base"
  }

  GetLogPrefix() {
    return `[${this.GetType()}][${this.name}]`;
  }

  Tick() {
    this.roomTickCount++
    
    log(`Tick ${this.GetType()} ${this.roomTickCount} of "${this.name}"`)
    log(`Players: %o`, this.players.map(p => `"${p.data.name}"`).join(', '))

    this.SendToRoom({type: 'tick'})
    this.CloseGameIfDone()
  }

  ReceiveFromPlayer(playerData, data) {
    let player = this.players.find(p => p.data.uuid == playerData.uuid)
    if(!player) logError(`received Message from player not in Game '${player.name}'   %o`, data)
    else{
      log(`received Message from player ${playerData.data.name}  %o`, data)
    }
  }

//    Messages to Players
  SetRoom(room) {
    this.roomSocket = room
  }

  SendToPlayer(idx, msg) {
    if(!this.players[idx]) {
      logError(`Trying to send to Player not in list`)
    }else{
      if(!this.players[idx].socket)
        logError(`Trying to send to Player with socket timeout`)
      else
        this.players[idx].socket.emit('game', msg)
    }
  }

  SendToRoom(msg) {
    try{
      log(`Send to room '${this.name}' %o`, msg)
      this.roomSocket.to(this.name).emit('game', msg)
    }catch(ex){
      logError(`Error sending to Room in Game '${this.name}'`)
    }
  }

// Joining and leaving of Players
  Join(playerData, socket) {
    log(`Adding Player ${playerData.name}  (${playerData.uuid})`)
    let alreadyIn = this.players.find(p => p.data.uuid == playerData.uuid)
    if(alreadyIn) {
      alreadyIn.socket = socket
    }
    else this.players.push({data: playerData, socket})
  }

  Leave(playerData, socket) {
    log(`Player ${playerData.name} left or socket timedout`)
    let found = this.players.find(p => p.data.uuid == playerData.uuid)
    if(found) found.socket = undefined

    this.CloseGameIfDone()
  }

  /**
   * Close Game if noones left
   */
  CloseGameIfDone() {
    let presentPlayers = 0
    this.players.forEach(p => {
      if(p.socket) presentPlayers++
    })

    if(presentPlayers == 0 ){
      this.Stop()
      //gameList[this.name] = undefined;
    }
  }

  Stop() {
    log(`Stopping Game "${this.name}"`)
    clearInterval(this.handleInterval)
    Game.Remove(this.name)
  }
}

module.exports = GameBase
