const log = require('debug')('Game')
const logError = require('debug')('Game:Error')
const {
  GameRoundbased,
  GAMESTATE 
} = require('./GameRoundbased')

let Game4wins_Base = require('./Game4wins_base')
class Game4wins extends GameRoundbased {
  constructor(name) {
    super(name, 2)
  }

  Reset() {
    super.Reset();
    if(!this.game) this.game = new Game4wins_Base();
    else this.game.Reset();

    this.tickData.field = this.game.GetField();
    this.gamestate = GAMESTATE.WAITING_FOR_PLAYERS;
  }

  GetType() {
    return "4wins"
  }

  GetActivePlayerIdx() {
    return this.game.GetPlayerActiveIdx();
  }

  Update() {
    this.tickData.field = this.game.GetField();
  }

  ReceiveFromPlayerIdx(playerIdx, data) {
    super.ReceiveFromPlayerIdx(playerIdx, data);

    if(!Number.isInteger(data)) {
      log(`${this.GetLogPrefix()}: data is not integer ${data}`);
    }else if(this.game.PickField(data, playerIdx)) {
      this.SendToPlayer(this.game.GetPlayerActiveIdx(), {type: 'info', msg: 'youre it'})
    }else{
      log(`${this.GetLogPrefix()}: Field already locked`);
    }
  }
}

module.exports = Game4wins
