const log = require('debug')('Game')
const logError = require('debug')('Game:Error')
const GameBase = require('./GameBase')

const GAMESTATE = {
  RESET: 'RESET',
  WAITING_FOR_PLAYERS: 'WAITING_FOR_PLAYERS',
  RUNNING: 'RUNNING'
};

class GameRoundbased extends GameBase {
  constructor(name, maxPlayers) {
    super(name)
    this.ActivePlayerIdx = 0;
    this.maxPlayers = maxPlayers;

    this.Reset();
  }

  GetActivePlayerIdx() {
    return this.ActivePlayerIdx;
  }

  Reset() {
    log(`${this.GetLogPrefix()}: Reset ${this.GetType()} "${this.name}"`);
    this.ActivePlayerIdx = Math.floor(Math.random() * Math.floor(this.maxPlayers));

    this.tickData = {
      type: 'tick',
      gamestate: this.gamestate,
      players: this.players.map(p => p.data.name)
    };
  }

  GetType() {
    return "GameRoundbased"
  }

  Tick() {
    this.roomTickCount++
    
    log(`${this.GetLogPrefix()}: Tick ${this.GetType()} ${this.roomTickCount} of "${this.name}"`)
    log(`${this.GetLogPrefix()}: Players: %o`, this.players.map(p => `"${p.data.name}"`).join(', '))

    this.tickData = {
      ... this.tickData,
      type: 'tick',
      number: this.roomTickCount,
      gamestate: this.gamestate,
      players: this.players.map(p => p.data.name)
    };

    switch(this.gamestate) {
      case GAMESTATE.RESET:
        this.Reset();
        this.gamestate = GAMESTATE.WAITING_FOR_PLAYERS;
        // Falling through
      case GAMESTATE.WAITING_FOR_PLAYERS:
        if(this.players.length >= this.maxPlayers){
          this.gamestate = GAMESTATE.RUNNING;
        }
        break;
      case GAMESTATE.RUNNING:
        this.tickData.activePlayer = this.players[this.GetActivePlayerIdx()].data.uuid;
        this.Update();
        break;
    }
    this.SendToRoom(this.tickData);
    this.CloseGameIfDone()
  }

  Update() { }

  ReceiveFromActivePlayer(player, data) {
    log(`${this.GetLogPrefix()}: received Message from active player ${player.data.name}  %o`, data)
  }

  ReceiveFromPassivePlayer(player, data) {
    log(`${this.GetLogPrefix()}: received Message from passive player ${player.data.name}  %o`, data)
  }
  
  ReceiveFromPlayerIdx(playerIdx, data) {
    log(`${this.GetLogPrefix()}: received Message from playerIdx '${playerIdx}'   %o`, data)
  }

  ReceiveFromPlayer(playerData, data) {
    let player = this.players.find(p => p.data.uuid == playerData.uuid)
    if(!player) {
      logError(`${this.GetLogPrefix()}: received Message from player not in Game '${playerData.name}'   %o`, data)
    }else{
      let playerIdx = this.players.indexOf(player);
      this.ReceiveFromPlayerIdx(playerIdx, data);

      if(data == 'reset') {
        this.gamestate = GAMESTATE.RESET;
        return;
      }else if(data == 'start') {
        this.gamestate = GAMESTATE.RUNNING;
        return;
      }else if(this.players[this.ActivePlayerIdx].data.uuid == playerData.uuid) {
        this.ReceiveFromActivePlayer(player, data);
      }else{
        this.ReceiveFromPassivePlayer(player, data);
      }
    }
  }
}

module.exports = { GameRoundbased, GAMESTATE }
