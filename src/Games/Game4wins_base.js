const log = require('debug')('TicTacToe')
const logError = require('debug')('TicTacToe:Error')

class Game4wins_Base {
  constructor() {
    this.Reset();
  }

  Reset() {
    this.field = [
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.'],
      ['.', '.', '.', '.', '.', '.', '.']
    ];
    this.activePlayer = Math.floor(Math.random() * Math.floor(this.GetMaxPlayers()));
  }

  GetType() {
    return "4wins"
  }

  GetField() {
    return this.field;
  }

  GetMaxPlayers() {
    return 2;
  }

  GetPlayerActiveIdx() {
    return this.activePlayer;
  }

  NextPlayer() {
    this.activePlayer++
    this.activePlayer %= this.GetMaxPlayers();
  }

  PickField(x, playerIdx) {
    if(playerIdx == this.GetPlayerActiveIdx()
        && x >= 0 
        && x <= this.field[0].length) {
      let depth = this.field.length -1;
      for(; depth > -1 && this.field[depth][x] != '.'; depth--);
      
      if(depth < 0) return false;
      
      this.field[depth][x] = ['X', 'O'][this.GetPlayerActiveIdx()];
      this.NextPlayer();
    }else{
      return false;
    }

    return true;
  }
}

module.exports = Game4wins_Base
