const log = require('debug')('Game')
const logError = require('debug')('Game:Error')
const GameBase = require('./GameBase')


class GameCatch extends GameBase {
  constructor(name) {
    super(name)
    this.ActivePlayerIdx = 0

    log(`Starting ${this.GetType()} "${this.name}"`)
  }

  GetType() {
    return "catch"
  }

  Tick() {
    this.roomTickCount++
    
    log(`Tick ${this.GetType()} ${this.roomTickCount} of "${this.name}"`)
    log(`Players: %o`, this.players.map(p => `"${p.data.name}"`).join(', '))


    this.SendToRoom({type: 'tick', number: this.roomTickCount, players: this.players.map(p => p.data.name)})
    this.SendToPlayer(this.ActivePlayerIdx, {type: 'info', msg: 'youre it'})
    this.CloseGameIfDone()
  }

  ReceiveFromPlayer(playerData, data) {
    let player = this.players.find(p => p.data.uuid == playerData.uuid)
    if(!player) logError(`received Message from player not in Game '${playerData.name}'   %o`, data)
    else{
      if(this.players[this.ActivePlayerIdx].name == player.name) {
        log(`received Message from active player ${player.name}  %o`, data)
        this.ActivePlayerIdx++
        this.ActivePlayerIdx %= this.players.length
        this.SendToPlayer(this.ActivePlayerIdx, {type: 'info', msg: 'youre it'})
      }else
        log(`received Message from passive player ${player.name}  %o`, data)
    }
  }
}

module.exports = GameCatch
